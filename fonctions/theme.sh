#!/usr/bin/zsh
# Author: nym0x
# Date: 26/09/2023

# Définition des variables globales
global_vars(){
    # Dossier de Kalinit
    pathToSource="$( cd $(dirname "${BASH_SOURCE[0]}") && pwd )"

    # Initialisation des couleurs
    GREEN="\033[0;32m"
    RED="\033[0;31m"
    BLUE='\033[0;34m'
    YELLOW='\033[0;33m'
    ENDCOLOR="\e[0m"
}


######################################
###############  MAIN  ###############
######################################

theme_setup(){

    global_vars

    echo " \n \n${YELLOW}#### Paramétrage du thème ####${ENDCOLOR} \n" ; sleep 1
    cd /tmp/kalinit/ressources/theme

    mkdir ~/.themes 2> /dev/null
    mkdir ~/.local/share/icons 2> /dev/null
    mkdir ~/.local/share/fonts 2> /dev/null


    # Dezippage du thème
    unzip -qq ./GTK-XFWM-Theme.zip -d ~/.themes && rm ./GTK-XFWM-Theme.zip && sudo mv ~/.themes/GTK-XFWM-Everblush-Theme/* ~/.themes/.
    unzip -qq ./Nordzy-cyan-dark-MOD.zip -d ~/.local/share/icons && rm ./Nordzy-cyan-dark-MOD.zip
    git clone https://github.com/alvatip/Radioactive-nord.git && cd Radioactive-nord && ./install.sh 
    cd /tmp/kalinit/ressources/theme
    unzip -qq ./fonts.zip -d ~/.local/share/fonts && rm ./fonts.zip
    sudo apt install -y qt5-style-kvantum qt5-style-kvantum-themes
    unzip -qq ./Kvantum-theme.zip -d ~/.config && rm ./Kvantum-theme.zip
    sudo cp -R ~/.themes/Everblush /usr/share/themes
    sudo cp -R ~/.local/share/icons/Nordzy-cyan-dark-MOD /usr/share/icons


    echo "1. Se rendre dans Applications par défaut, onglet Utilitaires et dans l'émulateur de terminal, choisir Terminal XFCE"
    echo "2. Se rendre dans Apparence, onglet Style et sélectionner Everblush_GTX_THEME"
    echo "3. Se rendre dans Apparence, onglet Icones et sélectionner Nordzy-cyan-dark-mod"
    echo "4. Se rendre dans Apparence, onglet Polices et sélectionner Roboto Regular en taille 10 (texte) / JetBrainsMono Nerd Font Mono Regular en taille 10 (mono)"
    echo "5. Se rendre dans Gestionnaire de fenêtres, onglet Style et sélectionner Everblush_xfwm"
    echo "6. Se rendre dans Souris et pavé tactile, onglet Thème et sélectionner Radioactive-nord"
    echo "7. Se rendre dans Bureau > Type d'icones = none"
    echo "7. Se rendre dans LightDM GTK + Greeter Settings, choisir le thème Everblush et le set d'icones Nordzy-cyan-dark-MOD. Pour la couleur de background, définir #232a2d"
}


