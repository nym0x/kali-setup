#!/usr/bin/zsh
# Author: nym0x
# Date: 26/09/2023

# Définition des variables globales
global_vars(){
    # Dossier de Kalinit
    pathToSource="$( cd $(dirname "${BASH_SOURCE[0]}") && pwd )"

    # Initialisation des couleurs
    GREEN="\033[0;32m"
    RED="\033[0;31m"
    BLUE='\033[0;34m'
    YELLOW='\033[0;33m'
    ENDCOLOR="\e[0m"
}

# Durcissement des droits de certains exécutables
binaries_permissions(){
    echo "${YELLOW}[*] Durcissement des droits de certains exécutables${ENDCOLOR}"
    sudo chmod 755 $(which su)
    sudo chmod u+s $(which su)
    sudo chmod 700 $(which htop)
    sudo chmod 700 $(which top)
    sudo chmod 700 $(which pgrep)
    sudo chmod 700 $(which dmesg)
    sudo chmod 700 $(which fuser)
    sudo chmod 700 $(which killall)
    sudo chmod 700 $(which lsof)
    sudo chmod 700 $(which users)
    sudo chmod 700 $(which w)
    sudo chmod 700 $(which wall)
    sudo chmod 700 $(which who)
    sudo chmod 700 $(which write)
    echo "${GREEN}[+] Durcissement des exécutables effectué avec succès${ENDCOLOR} \n" ; sleep 1
}

# Modification de l'UMASK à 077
umask_hardening(){
    echo "${YELLOW}[*] Durcissement de l'umask${ENDCOLOR}"
    echo "umask 077" >> ~/.zshrc && source ~/.zshrc
    echo "${GREEN}[+] Durcissement de l'umask effectué avec succès${ENDCOLOR} \n" ; sleep 1
}


######################################
###############  MAIN  ###############
######################################

hardening(){

    global_vars
    binaries_permissions
    umask_hardening


}