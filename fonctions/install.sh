#!/usr/bin/zsh
# Author: nym0x
# Date: 26/09/2023

# Définition des variables globales
global_vars(){
    # Dossier de Kalinit
    pathToSource="$( cd $(dirname "${BASH_SOURCE[0]}") && pwd )"

    # Initialisation des couleurs
    GREEN="\033[0;32m"
    RED="\033[0;31m"
    BLUE='\033[0;34m'
    YELLOW='\033[0;33m'
    ENDCOLOR="\e[0m"
}

# Création de l'arborescence
make_arbo(){
    echo "${YELLOW}[*] Création des différents dossier de l'arborescence${ENDCOLOR}"

    mkdir {~/Tools,~/VPN,~/VM,~/Scripts,~/Wordlists,~/Projets,~/.themes} 2>> /tmp/errors.txt 
    mkdir ~/Documents/BugBounty 2>> /tmp/errors.txt 
    mkdir ~/Documents/HackTheBox 2>> /tmp/errors.txt 
    mkdir ~/Documents/TryHackMe  2>> /tmp/errors.txt 
    mkdir ~/Documents/CryptoHack 2>> /tmp/errors.txt 
    mkdir ~/Documents/RootMe 2>> /tmp/errors.txt 
    rmdir ~/Public ~/Musique 2>> /tmp/errors.txt 

    echo "${GREEN}[+] Arborescence créée avec succès${ENDCOLOR} \n" ; sleep 1
}

# Devenir SUDO (et le rester)
get_sudo(){
    sudo -v # Demande le mot de passe SUDO
    while true; do sleep 60; sudo -n true; kill -0 "$$" || exit; done 2> /dev/null & 
}
# Firefox Setup
firefox_setup(){
    # Extensions Firefox
    echo "${YELLOW}[+] Ouverture des extensions intéressantes (bien penser à les installer)${ENDCOLOR}"
    open https://addons.mozilla.org/fr/firefox/addon/foxyproxy-standard/ 
    open https://addons.mozilla.org/fr/firefox/addon/pwnfox/
    open https://addons.mozilla.org/fr/firefox/addon/wappalyzer/
    open https://addons.mozilla.org/fr/firefox/addon/ublock-origin/
    open https://addons.mozilla.org/fr/firefox/addon/proton-pass/

    # Favoris 
    echo "${YELLOW}[+] Téléchargement des favoris (bien penser à les importer dans Firefox)${ENDCOLOR}"
    wget -q -N https://gitlab.com/nym0x/kalinit/-/raw/main/ressources/bookmarks.json
}


#########################################
###############  UPDATES  ###############
#########################################

update_apt(){
    # Mise à jour des dépots
    echo "${YELLOW}[*] Mise à jour des dépots${ENDCOLOR}"
    sudo apt -qq -y update 2>> /tmp/errors.txt 
    echo "${GREEN}[+] Dépots mis à jour avec succès${ENDCOLOR} \n" ; sleep 1

    # Installation des mises à jours
    echo "${YELLOW}[*] Installation des mises à jour${ENDCOLOR}"
    sudo apt -qq -y dist-upgrade 2>> /tmp/errors.txt
    sudo apt -qq -y upgrade 2>> /tmp/errors.txt
    echo "${GREEN}[+] Mises à jour installées avec succès${ENDCOLOR} \n" ; sleep 1
}


#########################################
###############  PAQUETS  ###############
#########################################

# Installation de diverses librairies
libraries_install(){
    echo "${YELLOW}[*] Installation des librairies${ENDCOLOR}"
    sudo apt install -qq -y apt-transport-https build-essential ca-certificates cifs-utils gnupg2 libffi-dev libkrb5-dev libpcap-dev net-tools nfs-common software-properties-common 2>> /tmp/errors.txt 
    echo "${GREEN}[+] Librairies installées avec succès${ENDCOLOR} \n" ; sleep 1
}

# Installation de divers packets
packets_install(){
    echo "${YELLOW}[*] Installation de divers packets${ENDCOLOR}"
    sudo apt install -qq -y \
        auditd \
        bettercap \
        bloodhound \
        bloodhound.py \
        bridge-utils \
        chromium \
        flameshot \
        git \
        gobuster \
        golang-go \
        neo4j \
        nuclei \
        postgresql \
        python3-venv \
        python3-dev \
        python3-pip \
        pipx \
        testssl.sh \
        rlwrap \
        ruby-full \
        xfce4-terminal \
        virtualbox \
        virtualbox-ext-pack \
        virtualbox-guest-additions-iso \
        vlc 
    sudo apt -qq -y --fix-broken install > /dev/null 2>> /tmp/errors.txt
    pipx ensurepath
    echo "${GREEN}[+] Packets installés avec succès${ENDCOLOR} \n" ; sleep 1
}


#############################################
###############  UTILITAIRES  ###############
#############################################

# Installation de BurpSuite
burpsuite_install(){
    echo "${YELLOW}[*] Installation de BurpSuite Professionnal${ENDCOLOR}"
    cd ~/Téléchargements
    sudo wget -q -N 'https://portswigger.net/burp/releases/startdownload?product=pro&version=2023.5.2&type=Linux' -O burpsuite-pro.sh > /dev/null 2>> /tmp/errors.txt
    sudo chmod +x ./burpsuite-pro.sh > /dev/null 2>> /tmp/errors.txt
    ./burpsuite-pro.sh 
    sudo rm ./burpsuite-pro.sh
    echo "${GREEN}[+] BurpSuite Professionnal a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # Téléchargement de Jython pour BurpSuite
    echo "${YELLOW}[+] Téléchargement de Jython${ENDCOLOR}"
    cd ~/Téléchargements
    sudo wget -q -N https://repo1.maven.org/maven2/org/python/jython-standalone/2.7.3/jython-standalone-2.7.3.jar
    echo "${GREEN}[+] Jython a été correctement téléchargé${ENDCOLOR} \n" ; sleep 1
}

# Installation d'Obsidian
obsidian_install(){
    echo "${YELLOW}[*] Installation d'Obsidian${ENDCOLOR}"
    cd ~/Téléchargements
    sudo wget -q -N https://github.com/obsidianmd/obsidian-releases/releases/download/v1.0.0/obsidian_1.0.0_amd64.deb > /dev/null 2>> /tmp/errors.txt
    sudo dpkg -i obsidian_1.0.0_amd64.deb > /dev/null 2>> /tmp/errors.txt
    sudo rm obsidian_1.0.0_amd64.deb > /dev/null 2>> /tmp/errors.txt
    echo "${GREEN}[+] Obsidian a été correctement installé${ENDCOLOR} \n" ; sleep 1
}

# Installation de Only Office
onlyoffice_install(){
    echo "${YELLOW}[*] Installation de Only Office${ENDCOLOR}"
    cd ~/Téléchargements
    sudo apt -qq -y --fix-broken install > /dev/null 2>> /tmp/errors.txt
    sudo wget -q -N https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors_amd64.deb > /dev/null 2>> /tmp/errors.txt
    sudo dpkg -i onlyoffice-desktopeditors_amd64.deb > /dev/null 2>> /tmp/errors.txt
    sudo rm onlyoffice-desktopeditors_amd64.deb > /dev/null 2>> /tmp/errors.txt
    echo "${GREEN}[+] Only Office a été correctement installé${ENDCOLOR} \n" ; sleep 1
}

# Installation de Proton VPN
protonvpn_install(){
    echo "${YELLOW}[*] Installation de Proton VPN${ENDCOLOR}"
    cd ~/Téléchargements
    sudo wget -q -N https://repo.protonvpn.com/debian/dists/stable/main/binary-all/protonvpn-stable-release_1.0.3_all.deb > /dev/null 2>> /tmp/errors.txt
    sudo dpkg -i protonvpn-stable-release_1.0.3_all.deb > /dev/null 2>> /tmp/errors.txt
    sudo rm protonvpn-stable-release_1.0.3_all.deb > /dev/null 2>> /tmp/errors.txt
    sudo apt -q update
    sudo apt -q -y install protonvpn-stable-release protonvpn > /dev/null 2>> /tmp/errors.txt
    echo "${GREEN}[+] Proton VPN a été correctement installé${ENDCOLOR} \n" ; sleep 1
}
    
# Installation de Visual Code Studio
visualcode_install(){
    echo "${YELLOW}[*] Installation de Visual Code Studio${ENDCOLOR}"
    cd ~/Téléchargements
    sudo wget -q -N 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -O visual-code-studio-latest.deb > /dev/null 2>> /tmp/errors.txt
    sudo dpkg -i visual-code-studio-latest.deb > /dev/null 2>> /tmp/errors.txt
    sudo rm visual-code-studio-latest.deb > /dev/null 2>> /tmp/errors.txt
    echo "${GREEN}[+] Visual Code Studio a été correctement installé${ENDCOLOR} \n" ; sleep 1
}


############################################
###############  DEPOTS GIT  ###############
############################################

# Installation de dépots GIT
gits_install(){

    cd /opt

    # Anew
    echo "${YELLOW}[*] Téléchargement de Anew${ENDCOLOR}"
    sudo wget -q -N https://github.com/tomnomnom/anew/releases/download/v0.1.1/anew-linux-amd64-0.1.1.tgz > /dev/null 2>> /tmp/errors.txt
    sudo tar -zxf anew-linux-amd64-0.1.1.tgz && sudo ln -s /opt/anew ~/Tools/anew
    sudo rm ./anew-linux-amd64-0.1.1.tgz
    echo "${GREEN}[+] Anew a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # Aquatone 
    echo "${YELLOW}[*] Téléchargement de Aquatone${ENDCOLOR}"
    sudo wget -q -N https://github.com/michenriksen/aquatone/releases/download/v1.7.0/aquatone_linux_amd64_1.7.0.zip > /dev/null 2>> /tmp/errors.txt
    sudo unzip -qq aquatone_linux_amd64_1.7.0.zip && sudo ln -s /opt/aquatone ~/Tools/aquatone
    sudo rm ./aquatone_linux_amd64_1.7.0.zip LICENSE.txt README.md
    echo "${GREEN}[+] Aquatone a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # TPLMap
    echo "${YELLOW}[*] Installation de TPLMap${ENDCOLOR}"
    sudo git clone --quiet --progress https://github.com/epinna/tplmap && sudo ln -s /opt/tplmap/tplmap.py ~/Tools/tplmap > /dev/null 2>> /tmp/errors.txt
    echo "${GREEN}[+] TPLMap a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # GitTools
    echo "${YELLOW}[*] Installation des GitTools${ENDCOLOR}"
    sudo git clone --quiet --progress https://github.com/internetwache/GitTools && sudo ln -s /opt/GitTools/Dumper/gitdumper.sh ~/Tools/gitdumper && sudo ln -s /opt/GitTools/Extractor/extractor.sh ~/Tools/gitextractor && sudo ln -s /opt/GitTools/Finder/gitfinder.py ~/Tools/gitfinder
    echo "${GREEN}[+] GitTools a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # Goddi
    echo "${YELLOW}[*] Téléchargement de Goddi${ENDCOLOR}"
    sudo wget -q -N https://github.com/NetSPI/goddi/releases/download/v1.2/goddi-linux-amd64 > /dev/null 2>> /tmp/errors.txt
    sudo ln -s /opt/goddi-linux-amd64 ~/Tools/goddi && sudo chmod +x /opt/goddi-linux-amd64
    echo "${GREEN}[+] Goddi a été correctement installé${ENDCOLOR} \n" ; sleep 1   

    # HTTPX
    echo "${YELLOW}[*] Téléchargement de HTTPX${ENDCOLOR}"
    sudo wget -q -N https://github.com/projectdiscovery/httpx/releases/download/v1.3.1/httpx_1.3.1_linux_amd64.zip > /dev/null 2>> /tmp/errors.txt
    sudo unzip -qq httpx_1.3.1_linux_amd64.zip && ln -s /opt/httpx ~/Tools/sub-httpx
    sudo rm httpx_1.3.1_linux_amd64.zip LICENSE.md README.md
    echo "${GREEN}[+] HTTPX a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # Impacket
    echo "${YELLOW}[*] Installation d'Impacket${ENDCOLOR}"
    python3 -m pipx install impacket
    echo "${GREEN}[+] Impacket a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # LaZagne
    echo "${YELLOW}[*] Téléchargement de LaZagne${ENDCOLOR}"
    sudo wget -q -N https://github.com/AlessandroZ/LaZagne/releases/download/v2.4.5/LaZagne.exe
    echo "${GREEN}[+] LaZagne a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # JWT Tool
    echo "${YELLOW}[*] Téléchargement de JWT Tool${ENDCOLOR}"
    sudo git clone --quiet --progress https://github.com/ticarpi/jwt_tool && sudo chmod +x /opt/jwt_tool/jwt_tool.py && sudo ln -s /opt/jwt_tool/jwt_tool.py ~/Tools/jwt_tool.py
    python3 -m pip install -q termcolor cprint pycryptodomex requests
    echo "${GREEN}[+] JWT Tool a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # Lsassy
    echo "${YELLOW}[*] Installation de Lsassy${ENDCOLOR}"
    python3 -m pip install lsassy
    echo "${GREEN}[+] Lsassy a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # MITM6
    echo "${YELLOW}[*] Installation de MITM6${ENDCOLOR}"
    sudo git clone --quiet --progress https://github.com/dirkjanm/mitm6 && cd mitm6
    sudo pip3 -q install -r requirements.txt && sudo python3 setup.py install 
    sudo ln -s /usr/local/bin/mitm6 ~/Tools/mitm6 && cd /opt
    echo "${GREEN}[+] MITM6 a été correctement installé${ENDCOLOR} \n" ; sleep 1

    # WayBackURLs
    echo "${YELLOW}[*] Téléchargement de WayBackURLs${ENDCOLOR}"
    sudo wget -q -N https://github.com/tomnomnom/waybackurls/releases/download/v0.1.0/waybackurls-linux-amd64-0.1.0.tgz > /dev/null
    sudo tar -zxf waybackurls-linux-amd64-0.1.0.tgz && ln -s /opt/waybackurls ~/Tools/waybackurls
    sudo rm waybackurls-linux-amd64-0.1.0.tgz
    echo "${GREEN}[+] WayBackURLs a été correctement installé${ENDCOLOR} \n" ; sleep 1
}


#########################################
##############  WORDLISTS  ##############
#########################################

wordlists_install(){
    cd ~/Wordlists

    echo " ${YELLOW}Téléchargement des wordlists${ENDCOLOR}" ; sleep 1
    
    git clone --quiet --progress https://github.com/danielmiessler/SecLists && sudo ln -s ~/Wordlists/SecLists /usr/share/wordlists/SecLists
    git clone --quiet --progress https://github.com/swisskyrepo/PayloadsAllTheThings
    git clone --quiet --progress https://github.com/clem9669/wordlists
    git clone --quiet --progress https://github.com/praetorian-inc/Hob0Rules
    git clone --quiet --progress https://github.com/Bo0oM/fuzz.txt.git

    wget -q -N https://raw.githubusercontent.com/NotSoSecure/password_cracking_rules/master/OneRuleToRuleThemAll.rule

    echo "${GREEN}[+] Les wordlists ont été correctement installées${ENDCOLOR} \n" ; sleep 1
}


######################################
###############  MAIN  ###############
######################################

fresh_install(){
    # Création du fichier d'erreurs
    touch /tmp/errors.txt 

    # Fonctions d'installation
    get_sudo
    global_vars
    update_apt
    make_arbo
    libraries_install
    packets_install
    gits_install
    visualcode_install
    burpsuite_install
    obsidian_install
    #onlyoffice_install
    protonvpn_install
    wordlists_install

    # Ajout du répertoire Tools dans le PATH
    echo "${GREEN}[+] Ajout de répertoires dans le PATH${ENDCOLOR} \n"
    echo 'export PATH=~/Tools:$PATH' >> ~/.zshrc 2>> /tmp/errors.txt 
    echo 'export PATH=~/.local/bin:$PATH' >> ~/.zshrc 2>> /tmp/errors.txt 
    source ~/.zshrc

    # Suppression des packets pas utilisés
    echo "${YELLOW}[*] Suppression des packets pas utilisés${ENDCOLOR}"
    sudo apt -qq -y autoremove 2>> /tmp/errors.txt
    echo "${GREEN}[+] Packets obsolètes supprimés avec succès${ENDCOLOR} \n" ; sleep 1

    firefox_setup

    # Alias
    echo " \n \n${YELLOW}[*] Ajout des alias${ENDCOLOR} \n" ; sleep 1
    cat $pathToSource/kalinit/ressources/aliases.txt >> ~/.zshrc
    source ~/.zshrc
}


######################################
###############  FIN !  ##############
######################################