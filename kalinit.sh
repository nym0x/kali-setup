#!/usr/bin/zsh
# Author: nym0x
# Date: 08/08/2023

# Ajouter une banner pour pimper le tout !

source ./fonctions/install.sh
source ./fonctions/hardening.sh
source ./fonctions/theme.sh

fresh_install
hardening
theme_setup